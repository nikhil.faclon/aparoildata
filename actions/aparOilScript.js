import { db, mqttClient } from '../app.js';
import { sendErrorMail } from '../util/sendErrorMail.js';
import moment from 'moment';
import schedule from 'node-schedule';

const sleep = (time) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {resolve();}, time * 1000);
    });
}

const fetchData = async (retry) => {
    try {
        console.log(retry);
        const date = moment().format('YYYY-MM-DD');
        // columns
        const columns = `po_no, updated_time, material_type_id, quantity`;
        const result = await db.query(`SELECT ${columns} FROM aparOil WHERE updated_time < ?`, [date]);
        return result[0];
    } catch (error) {
        if(retry['attempt'] < retry['maxAttempts']){
            retry['attempt'] += 1;
            await sleep(3);
            return fetchData(retry);
        } else {
            sendErrorMail(error.message);
            throw error;
        }
    }
};

const getDataInInterval = async () => {
    try {
        const intervalInMinutes = process.env.INTERVAL;
        schedule.scheduleJob('*/10 * * * *', async() => {
            try {
                const retry = {
                    attempt : 0,
                    maxAttempts : 2,
                };
    
                const data = await fetchData(retry);
                console.log(data);
    
                if(!data)
                    throw error;
    
                const device = "APARVMS";

                for(let i = 0; i < data.length; i++ ) {

                }
    
                // multiple datapackets , no processing
                const dataPacket = {
                    device,
                    data : [
                                {"tag" : "po_no", "value" : 121516},
                                {"tag" : "updated_time", "value" : "11/20/2018  3:47:59 PM"},
                                {"tag" : "material_type_id", "value" : 2},
                                {"tag" : "status" , "value" : 1},
                                {"tag" : "RSSI" , "value" : 22}
                    ]     
                };
                const topic = `devicesIn/${device}/aparOilDataNikhil`;
                // mqttClient.publish(topic, JSON.stringify(dataPacket));
            } catch (error) {
                throw error;
            }
        });
    } catch (error) {
        console.log(error);
    }
};

export {
    getDataInInterval,
}