import nodemailer from 'nodemailer';

const mailEmailList = ['nikhil.faclon@gmail.com'];
const mailCredentials = {
    host: 'smtp.gmail.com',
    ssl: true,
    secure: true,
    port: 465,
    auth: {
        user: 'errormail.faclon@gmail.com',
        pass: 'idrjkrvhneymueji'
    }
};
const sendErrorMail = (errorMsg) => {
    const subject = `Database Connection Failure`;
    const options = {
        from: `Faclon Labs <${mailCredentials.auth.user}>`,
        to: mailEmailList,
        subject: subject,
        text: `${errorMsg}, Server: azure_reportGen Server 4.224.31.73`
    };
    const tranporter = nodemailer.createTransport(mailCredentials);
    const mailOptions = options;
    tranporter.sendMail(mailOptions, (error) => {
        if (error) {
            console.error('Error in sending error mail', error);
        } else {
            console.log('Database error mail sent successfully');
        }
    });
}

export {
    sendErrorMail,
}