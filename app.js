import dotenv from 'dotenv';
import mysql from 'mysql2';
import mqtt from 'mqtt';
import { getDataInInterval } from './actions/aparOilScript.js';

dotenv.config();

const db = mysql.createPool({
    user: process.env.MYSQL_USER,
    host: process.env.MYSQL_HOST,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB,
}).promise();

const mqttClient = mqtt.connect({
    host: process.env.MQTT_HOST,
    port: process.env.MQTT_PORT,
    clientId: process.env.MQTT_CLIENTID,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
    qos: process.env.MQTT_QOS
});

mqttClient.on('connect', () => {
    console.log('MQTT connected');
});

// when mqtt disconnect restart
mqttClient.on('disconnect', () => {
    process.exit();
});

getDataInInterval();

export { 
    db,
    mqttClient,
};